<?php
return array (
    'name' => 'search',
    'type' => 'controller',
    'technology' => 'web',
    'mapped_to' => 'search',
    'storage_role' => 'root',
    'children' => 
    array (
        'index' => 
        array (
            'name' => 'index',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'index',
            'fields' => 
            array (
                0 => 
                array (
                    'name' => 'searchString',
                    'source' => 'query',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'SQL' => 
                            array (
                                'enabled' => false,
                                'blind' => true,
                            ),
                        ),
                    ),
                ),
                1 => 
                array (
                    'name' => 'id',
                    'source' => 'query',
                ),
                2 => 
                array (
                    'name' => 'brands',
                    'source' => 'query',
                ),
                3 => 
                array (
                    'name' => 'price',
                    'source' => 'query',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'IntegerOverflow' => 
                            array (
                                'enabled' => true,
                                'transform_strategy' => 'cast_to_integer',
                                'custom_value' => 0,
                                'action_on_not_numeric' => 'bypass',
                            ),
                        ),
                    ),
                ),
                4 => 
                array (
                    'name' => 'quality',
                    'source' => 'query',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'IntegerOverflow' => 
                            array (
                                'enabled' => true,
                                'transform_strategy' => 'cast_to_integer',
                                'custom_value' => 0,
                                'action_on_not_numeric' => 'bypass',
                            ),
                        ),
                    ),
                ),
                5 => 
                array (
                    'name' => 'page',
                    'source' => 'query',
                ),
            ),
        ),
    ),
);