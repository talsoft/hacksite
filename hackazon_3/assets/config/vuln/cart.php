<?php
return array (
    'name' => 'cart',
    'type' => 'controller',
    'technology' => 'web',
    'mapped_to' => 'cart',
    'storage_role' => 'root',
    'fields' => 
    array (
        0 => 
        array (
            'name' => 'qty',
            'source' => 'body',
            'vulnerabilities' => 
            array (
                'vuln_list' => 
                array (
                    'IntegerOverflow' => 
                    array (
                        'enabled' => true,
                        'transform_strategy' => 'cast_to_integer',
                        'custom_value' => 0,
                        'action_on_not_numeric' => 'bypass',
                    ),
                ),
            ),
        ),
        1 => 
        array (
            'name' => 'product_id',
            'source' => 'body',
        ),
        2 => 
        array (
            'name' => 'itemId',
            'source' => 'any',
            'vulnerabilities' => 
            array (
                'vuln_list' => 
                array (
                    'IntegerOverflow' => 
                    array (
                        'enabled' => true,
                        'transform_strategy' => 'cast_to_integer',
                        'custom_value' => 0,
                        'action_on_not_numeric' => 'bypass',
                    ),
                ),
            ),
        ),
    ),
    'children' => 
    array (
        'add' => 
        array (
            'name' => 'add',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'add',
            'fields' => 
            array (
                0 => 
                array (
                    'name' => 'product_id',
                    'source' => 'body',
                ),
            ),
        ),
        'view' => 
        array (
            'name' => 'view',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'view',
            'vulnerabilities' => 
            array (
                'vuln_list' => 
                array (
                    'PHPSessionIdOverflow' => 
                    array (
                        'enabled' => true,
                        'on_corrupted_id' => 'fix',
                    ),
                    'RemoteFileInclude' => 
                    array (
                        'enabled' => true,
                    ),
                ),
            ),
        ),
        'update' => 
        array (
            'name' => 'update',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'update',
        ),
        'empty' => 
        array (
            'name' => 'empty',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'empty',
        ),
        'setMethods' => 
        array (
            'name' => 'setMethods',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'setMethods',
            'fields' => 
            array (
                0 => 
                array (
                    'name' => 'shipping_method',
                    'source' => 'body',
                ),
                1 => 
                array (
                    'name' => 'payment_method',
                    'source' => 'body',
                ),
                2 => 
                array (
                    'name' => 'credit_card_number',
                    'source' => 'body',
                ),
                3 => 
                array (
                    'name' => 'credit_card_year',
                    'source' => 'body',
                ),
                4 => 
                array (
                    'name' => 'credit_card_month',
                    'source' => 'body',
                ),
                5 => 
                array (
                    'name' => 'credit_card_cvv',
                    'source' => 'body',
                ),
            ),
        ),
    ),
);