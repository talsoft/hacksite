<?php
return array (
    'name' => 'home',
    'type' => 'controller',
    'technology' => 'web',
    'mapped_to' => 'home',
    'storage_role' => 'root',
    'fields' => 
    array (
        0 => 
        array (
            'name' => 'visited_products',
            'source' => 'cookie',
            'vulnerabilities' => 
            array (
                'vuln_list' => 
                array (
                    'IntegerOverflow' => 
                    array (
                        'enabled' => true,
                        'transform_strategy' => 'cast_to_integer',
                        'custom_value' => 0,
                        'action_on_not_numeric' => 'bypass',
                    ),
                    'SQL' => 
                    array (
                        'enabled' => false,
                        'blind' => false,
                    ),
                ),
            ),
        ),
    ),
    'children' => 
    array (
        'index' => 
        array (
            'name' => 'index',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'index',
            'vulnerabilities' => 
            array (
                'vuln_list' => 
                array (
                    'CSRF' => 
                    array (
                        'enabled' => true,
                    ),
                    'PHPSessionIdOverflow' => 
                    array (
                        'enabled' => true,
                        'on_corrupted_id' => 'fix',
                    ),
                    'Referer' => 
                    array (
                        'enabled' => true,
                    ),
                ),
            ),
        ),
    ),
);