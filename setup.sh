#!/bin/bash
#  Copyright (C) 2015  Authors, TALSOFT TS
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# TALSOFT TS - www.talsoft.com.ar - Mar del Plata, Argentina

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

#echo "Download Hackazon Talsoft for kali"
#git clone https://talsoft@bitbucket.org/talsoft/hacksite.git hacksite
echo -n "Enter number of your app test and press [ENTER]: "
read NUM_SITE

echo "Copying hacksite"
cp -r hackazon_${NUM_SITE} /var/www/

echo "Change folder permissions"
chown -R www-data:www-data /var/www/hackazon_${NUM_SITE}/assets/config
chown -R www-data:www-data /var/www/hackazon_${NUM_SITE}/web/upload
chown -R www-data:www-data /var/www/hackazon_${NUM_SITE}/web/products_pictures/

echo "Change permision for upload files"
chmod 777 -R /var/www/hackazon_${NUM_SITE}

echo "Install Flash plugin"
apt-get install flashplugin-nonfree  php-bcmath php-mbstring

echo "Restarting Apache and Mysql"
a2enmod rewrite
service apache2 restart
service mysql restart

echo "Install db"
mysqladmin -u root password toor
mysql -u root -ptoor -e "CREATE DATABASE hackazon_"${NUM_SITE}"; FLUSH PRIVILEGES;"
mysql -u root -ptoor -e "CREATE USER 'hackazon'@'localhost' IDENTIFIED BY 'hackazon';"
mysql -u root -ptoor -e "GRANT ALL PRIVILEGES ON * . * TO 'hackazon'@'localhost';"

echo "import DBs" 
mysql -u root -ptoor hackazon_${NUM_SITE} < scripts/hackazon_${NUM_SITE}.sql

echo "Writing settings Apache2"
touch /etc/apache2/sites-enabled/hackazon_${NUM_SITE}.conf
echo "<VirtualHost *:80>
        ServerName hackazon_${NUM_SITE}

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/hackazon_${NUM_SITE}/web/
        <Directory />
                Options FollowSymLinks
                AllowOverride All
        </Directory>
        <Directory /var/www/hackazon_${NUM_SITE}/web/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ErrorLog /var/log/apache2/error.log
        CustomLog /var/log/apache2/access.log combined
</VirtualHost>" | tee /etc/apache2/sites-enabled/hackazon_${NUM_SITE}.conf

echo "Add hosts file"
IP=$(hostname -I | cut -d' ' -f1)
echo "${IP} hackazon_${NUM_SITE}"  | tee -a /etc/hosts
echo "Restaring Apache2"
/etc/init.d/apache2 restart
echo "Finish!!!"
echo "Browser http://hackazon_${NUM_SITE}/ and Go!!!"
