<?php
return array (
    'name' => 'review',
    'type' => 'controller',
    'technology' => 'web',
    'mapped_to' => 'review',
    'storage_role' => 'root',
    'fields' => 
    array (
        0 => 
        array (
            'name' => 'secret_data',
            'source' => 'query',
            'vulnerabilities' => 
            array (
                'vuln_list' => 
                array (
                    'OSCommand' => 
                    array (
                        'enabled' => true,
                    ),
                ),
            ),
        ),
    ),
    'vulnerabilities' => 
    array (
        'vuln_list' => 
        array (
            'CSRF' => 
            array (
                'enabled' => true,
            ),
        ),
    ),
    'children' => 
    array (
        'send' => 
        array (
            'name' => 'send',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'send',
            'fields' => 
            array (
                0 => 
                array (
                    'name' => 'productID',
                    'source' => 'body',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'IntegerOverflow' => 
                            array (
                                'enabled' => true,
                                'transform_strategy' => 'cast_to_integer',
                                'custom_value' => 0,
                                'action_on_not_numeric' => 'bypass',
                            ),
                        ),
                    ),
                ),
                1 => 
                array (
                    'name' => 'userName',
                    'source' => 'body',
                ),
                2 => 
                array (
                    'name' => 'starValue',
                    'source' => 'body',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'IntegerOverflow' => 
                            array (
                                'enabled' => true,
                                'transform_strategy' => 'cast_to_integer',
                                'custom_value' => 0,
                                'action_on_not_numeric' => 'bypass',
                            ),
                        ),
                    ),
                ),
                3 => 
                array (
                    'name' => 'textReview',
                    'source' => 'body',
                ),
                4 => 
                array (
                    'name' => 'userEmail',
                    'source' => 'body',
                ),
            ),
        ),
    ),
);