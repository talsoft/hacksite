<?php
return array (
    'name' => 'category',
    'type' => 'controller',
    'technology' => 'generic',
    'mapped_to' => 'category',
    'storage_role' => 'root',
    'children' => 
    array (
        'view' => 
        array (
            'name' => 'view',
            'type' => 'action',
            'technology' => 'generic',
            'mapped_to' => 'view',
            'fields' => 
            array (
                0 => 
                array (
                    'name' => 'id',
                    'source' => 'query',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'IntegerOverflow' => 
                            array (
                                'enabled' => true,
                                'transform_strategy' => 'cast_to_integer',
                                'custom_value' => 0,
                                'action_on_not_numeric' => 'bypass',
                            ),
                            'SQL' => 
                            array (
                                'enabled' => false,
                                'blind' => true,
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);