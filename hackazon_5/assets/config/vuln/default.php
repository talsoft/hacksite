<?php
return array (
    'name' => 'default',
    'type' => 'application',
    'technology' => 'generic',
    'mapped_to' => 'default',
    'storage_role' => 'root',
    'vulnerabilities' => 
    array (
        'vuln_list' => 
        array (
            'CSRF' => 
            array (
                'enabled' => true,
            ),
            'PHPSessionIdOverflow' => 
            array (
                'enabled' => true,
                'on_corrupted_id' => 'fix',
            ),
            'Referer' => 
            array (
                'enabled' => true,
            ),
        ),
    ),
);